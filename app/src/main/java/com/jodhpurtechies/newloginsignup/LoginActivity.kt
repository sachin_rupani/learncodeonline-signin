package com.jodhpurtechies.newloginsignup

import android.graphics.Color
import android.os.Bundle
import android.os.CountDownTimer
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.AppCompatTextView
import android.view.View
import android.view.ViewAnimationUtils
import android.view.Window
import android.view.WindowManager
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.ScaleAnimation
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.GlideDrawable
import com.bumptech.glide.request.animation.GlideAnimation
import com.bumptech.glide.request.target.SimpleTarget
import com.jodhpurtechies.newloginsignup.fragments.LoginFragment
import com.jodhpurtechies.newloginsignup.fragments.SignUpFragment
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_login)

        onClickListeners()

        //circular animate layout
        linMain.post {
            circularAnimateView(linMain)
        }
    }

    private fun circularAnimateView(myView: View) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            val cx = myView.right
            val cy = myView.bottom

            val dx = Math.max(cx, myView.getWidth() - cx)
            val dy = Math.max(cy, myView.getHeight() - cy)
            val finalRadius = Math.hypot(dx.toDouble(), dy.toDouble()).toFloat()

            val animator = ViewAnimationUtils.createCircularReveal(myView, cx, cy, 0f, finalRadius)
            animator!!.interpolator = AccelerateDecelerateInterpolator()
            animator.duration = 2000
            animator.start()
        } else {
            myView.startAnimation(AnimationUtils.loadAnimation(applicationContext, R.anim.fade_in))
        }

        //wait and then begin with learn code online logo loading and animate
        object : CountDownTimer(2500, 2500) {
            override fun onFinish() {
                Glide.with(applicationContext).load(R.drawable.app_logo).into(object : SimpleTarget<GlideDrawable>() {
                    override fun onResourceReady(resource: GlideDrawable?, glideAnimation: GlideAnimation<in GlideDrawable>?) {
                        imgLogo.visibility = View.VISIBLE
                        imgLogo.setImageDrawable(resource)
                        imgLogo.startAnimation(AnimationUtils.loadAnimation(applicationContext, R.anim.zoom_in_overshoot))
                        startMainTaskNow()
                    }

                })
            }

            override fun onTick(p0: Long) {

            }

        }.start()
    }


    private fun startMainTaskNow() {
        txtTabSignIn.visibility = View.VISIBLE
        txtTabSignUp.visibility = View.VISIBLE

        val animFadeIn = AnimationUtils.loadAnimation(applicationContext, R.anim.fade_in)
        txtTabSignIn.startAnimation(animFadeIn)
        txtTabSignUp.startAnimation(animFadeIn)
        object : CountDownTimer(1200, 1200) {
            override fun onTick(p0: Long) {

            }

            override fun onFinish() {
                txtTabSignIn.performClick()
            }
        }.start()
    }




    private fun onClickListeners() {
        txtTabSignIn.setOnClickListener {
            switchTab(ContextCompat.getColor(applicationContext, R.color.colorPrimary), txtTabSignIn, R.drawable.bg_left_sign_in)
            switchTab(Color.parseColor("#ccffffff"), txtTabSignUp, android.R.color.transparent)
            initFragment(LoginFragment())
        }

        txtTabSignUp.setOnClickListener {
            switchTab(ContextCompat.getColor(applicationContext, R.color.colorPrimary), txtTabSignUp, R.drawable.bg_right_sign_up)
            switchTab(Color.parseColor("#ccffffff"), txtTabSignIn, android.R.color.transparent)
            initFragment(SignUpFragment())
        }
    }

    private fun switchTab(textColor: Int, textView: AppCompatTextView, bgRes: Int) {
        textView.setTextColor(textColor)
        textView.setBackgroundResource(bgRes)
    }

    private fun initFragment(mFragment: Fragment) {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.frameLayout, mFragment)
        fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
        fragmentTransaction.commit()
    }

    //for fragment components scale animation, one static method for both
    companion object {
        fun startViewScaleAnimation(viewToAnimate: View, duration: Long) {
            val anim = ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f)
            anim.duration = duration
            viewToAnimate.startAnimation(anim)

        }
    }

}
