package com.jodhpurtechies.newloginsignup.customViews

import android.content.Context
import android.support.v7.widget.AppCompatTextView
import android.util.AttributeSet
import com.jodhpurtechies.newloginsignup.custom.CustomFont


class MyTextViewBold : AppCompatTextView {
    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    private fun init() {
        val tf = CustomFont.setFontBold(context.assets)
        typeface = tf
    }
}