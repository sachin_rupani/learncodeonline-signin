package com.jodhpurtechies.newloginsignup.fragments

import android.content.Context
import android.os.Bundle
import android.os.CountDownTimer
import android.support.v4.app.Fragment
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.view.inputmethod.EditorInfo
import com.jodhpurtechies.newloginsignup.LoginActivity
import com.jodhpurtechies.newloginsignup.R
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.android.synthetic.main.fragment_login.view.*


class LoginFragment : Fragment() {

    private var contextAct: Context? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        this.contextAct = context
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_login, container, false)
        view.etPass.transformationMethod=PasswordTransformationMethod()
        startAnimation(view)
        return view
    }

    private fun startAnimation(view: View) {
        LoginActivity.startViewScaleAnimation(view.inpEmail, 500)
        LoginActivity.startViewScaleAnimation(view.inpPassword, 700)
        LoginActivity.startViewScaleAnimation(view.txtForgotPass, 800)
        LoginActivity.startViewScaleAnimation(view.btnSignIn, 1100)


        object : CountDownTimer(1200,1200){
            override fun onTick(p0: Long) {

            }

            override fun onFinish() {
                startSocialBtnAnimation(view.imgGoogle)
            }
        }.start()

    }

    private fun startSocialBtnAnimation(view: View, count: Int = 1) {
        view.visibility=View.VISIBLE
        view.startAnimation(AnimationUtils.loadAnimation(contextAct, R.anim.fade_in))
        object : CountDownTimer(500, 500) {
            override fun onTick(p0: Long) {

            }

            override fun onFinish() {
                if (count == 1) {
                    startSocialBtnAnimation(imgFacebook, 2)
                }
            }
        }.start()
    }


}