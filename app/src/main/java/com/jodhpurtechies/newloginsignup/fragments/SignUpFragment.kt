package com.jodhpurtechies.newloginsignup.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import com.jodhpurtechies.newloginsignup.LoginActivity
import com.jodhpurtechies.newloginsignup.R
import kotlinx.android.synthetic.main.fragment_sign_up.view.*


class SignUpFragment:Fragment(){
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view=inflater.inflate(R.layout.fragment_sign_up,container,false)
        view.etPass.transformationMethod=PasswordTransformationMethod()
        startAnimation(view)
        return view
    }

    private fun startAnimation(view: View) {
        LoginActivity.startViewScaleAnimation(view.inpName, 500)
        LoginActivity.startViewScaleAnimation(view.inpEmail, 700)
        LoginActivity.startViewScaleAnimation(view.inpMobile, 900)
        LoginActivity.startViewScaleAnimation(view.inpPassword, 1100)
        LoginActivity.startViewScaleAnimation(view.btnSignUp, 1300)
    }
}