package com.jodhpurtechies.newloginsignup.custom

import android.content.res.AssetManager
import android.graphics.Typeface


object CustomFont{
    fun setFontRegular(assetManager: AssetManager): Typeface {
        return Typeface.createFromAsset(assetManager, "fonts/muli-regular.ttf")
    }

    fun setFontBold(assetManager: AssetManager): Typeface {
        return Typeface.createFromAsset(assetManager, "fonts/muli-bold.ttf")
    }
}